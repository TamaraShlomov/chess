﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chess
{
    class Pawn : Piece //רגלי
    {
        static bool PreviousTurnGo2Steps ;
        public static Pawn prevPawn;  ///// שומר מצביע לרגלי שהלך 2 צעדים

        public Pawn(string symbol, bool black, int row, int col, Piece[,] board)
            : base(symbol, black, row, col, board)
        {

        }
       
        public static bool getPreviousTurn2Steps()
        {
            return PreviousTurnGo2Steps;
        }
       
        public static void setPreviousTurn2Steps(bool value)
        {
            PreviousTurnGo2Steps = value; ;
        }
       
        public override bool CanMoveTo(int row, int column)
        {
            if (CanDoEn_passant(row, column))
                return true;

            if (black)
            {
                if (column == this.column)
                {
                    if (row - this.row != 1 && row - this.row != 2)
                        return false;
                    if (this.row != 1 && row - this.row == 2)
                        return false;
                    if (board[row, column] != null)
                        return false;
                    if (row - this.row == 2 && board[row - 1, column] != null)
                        return false;
                    
                }
                else
                {  
                        if (Math.Abs(this.column - column) != 1 || row - this.row != 1)
                            return false;
                        if (board[row, column] == null)
                            return false;
                        if (board[row, column].Isblack() == this.Isblack())
                            return false;       
                }
            }
            else
            {
                if (column == this.column)
                {
                    if (row - this.row != -1 && row - this.row != -2)
                        return false;
                    if (this.row != 6 && row - this.row == -2)
                        return false;
                    if (board[row, column] != null)
                        return false;
                    if (row - this.row == -2 && board[row + 1, column] != null)
                        return false;
                    
                }
                else
                {      
                        if (Math.Abs(this.column - column) != 1 || row - this.row != -1)
                            return false;
                        if (board[row, column] == null)
                            return false;
                        if (board[row, column].Isblack() == this.Isblack())
                            return false;
                }
            }

            return true;
        }

        public bool CanDoEn_passant(int row,int column)
        {

            if (PreviousTurnGo2Steps)
            {
                if (black)
                {
                    if (this.row != row - 1 || Math.Abs(this.column - column) != 1)
                        return false;
                    if (!(board[row - 1, column] != null && (board[row - 1, column] is Pawn) && board[row - 1, column].Isblack() != this.Isblack()))
                        return false;
                    if (board[row - 1, column] != prevPawn)
                        return false;
                }
                else
                {
                    if (this.row != row + 1 || Math.Abs(this.column - column) != 1)
                        return false;
                    if (!(board[row + 1, column] != null && (board[row + 1, column] is Pawn) && board[row + 1, column].Isblack() != this.Isblack()))
                        return false;
                    if (board[row + 1, column] != prevPawn)
                        return false;
                }
            }
            else
                return false;

            return true;
            
        }
       
        public override void Move(int row2, int col2)
        {
            if (col2 == this.column)
            {
                if (black)
                    PreviousTurnGo2Steps = row2 - this.row == 2 ? true : false;
                else
                    PreviousTurnGo2Steps = row2 - this.row == -2 ? true : false;
                
                if(PreviousTurnGo2Steps)
                prevPawn =(Pawn)board[row,column] ; ///
            }
            else
            {
                if (CanDoEn_passant(row2, col2)) //
                board[black ? row2 - 1 : row2 + 1, col2] = null; ;

                PreviousTurnGo2Steps = !PreviousTurnGo2Steps; ////
            }
                       
            base.Move(row2, col2);
           
        }

    }


}
