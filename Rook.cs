﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chess
{
    class Rook : Piece //צריח
    {
        bool moved;

        public Rook(string symbol, bool black, int row, int col, Piece[,] board) : base(symbol, black, row, col, board) { }

        public bool Ismoved()
        {
            return moved;
        }
        
        public void setMoved(bool value)
        {
            moved = value;
        }
        
        public override bool CanMoveTo(int row, int column)
        {
            if (this.row != row && this.column != column)
                return false;

            if (board[row, column] != null && board[row, column].Isblack() == this.Isblack()) //&& הוא בסדר קדומיות נמוכה, אז קודם יבצע את את המשפט הראשון ,במידה ונכון ימשיך לבדוק את השני
                return false;
            //עכשיו בדיקה שלא היה לי אף אחד בדרך
            if (this.row == row)
            {
               
                int minCol = this.column < column ? this.column : column;
                int maxCol = this.column > column ? this.column : column;
                for (minCol = minCol + 1; minCol < maxCol; minCol++)
                    if (board[row, minCol] != null)
                        return false;
            }
            else
            {
                int minRow = this.row < row ? this.row : row;
                int maxRow = this.row > row ? this.row : row;
                for (minRow = minRow + 1; minRow < maxRow; minRow++)
                    if (board[minRow, column] != null)
                        return false;
            }
            return true;
        }
      
        public override void Move(int row2, int col2)
        {
            base.Move(row2, col2);
            moved = true;
        }
    }
}
