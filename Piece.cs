﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chess
{
    class Piece
    {
        protected string symbol;
        protected bool black;
        protected int row;
        protected int column;
        protected Piece[,] board;

        //public Piece() { } 
       
        public Piece(string symbol, bool black, int row, int column,Piece[,] board)
        {
            this.symbol = symbol;
            this.black = black;
            this.row = row;
            this.column = column;
            this.board = board;
        }

        public string getSymbol()
        {
            return symbol;
        }
        public bool Isblack()
        {
            return black;
        }
        public int getRow()
        {
            return row;
        }
        public int getColumn()
        {
            return column;
        }
        
        public virtual bool CanMoveTo(int row, int column)
        {
            return false;
        }
        
        public virtual void Move(int row2, int col2)
        {
           
            board[row2, col2] = board[row, column]; //זה מה שמשנה לי ויזואלית בלוח 
            board[row, column] = null; //זה מה שמשנה לי ויזואלית בלוח
            this.row = row2; //זה מה שבאמת משנה לי את המיקום באובייקט
            this.column = col2;
        }

       
    }
}
