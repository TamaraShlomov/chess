﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chess
{
    class Knight:Piece //פרש
    {
         public Knight(string symbol, bool black, int row, int col,Piece[,] board) : base(symbol, black, row, col,board) { }

         public override bool CanMoveTo(int row, int column)
         {
             if (Math.Abs(this.row - row) != 1 && Math.Abs(this.row - row) != 2)
                 return false;

             if (Math.Abs(this.row - row) == 1)
                 if (Math.Abs(this.column - column) != 2)
                     return false;

             if (Math.Abs(this.row - row) == 2)
                 if (Math.Abs(this.column - column) != 1)
                     return false;

             return true;
         }
    }
}
