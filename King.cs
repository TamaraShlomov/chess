﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chess
{
    class King : Piece
    {
        bool moved;

        public King(string symbol, bool black, int row, int col, Piece[,] board) : base(symbol, black, row, col, board) { }

        public bool Ismoved()
        {
            return moved;
        }
        
        public void setMoved(bool value)
        {
            moved = value;
        }
         
        public override bool CanMoveTo(int row, int column)
        {
            if (row != this.row + 1 && row != this.row - 1 && row != this.row)
                return false;
            if (column != (this.column + 1) && column != (this.column - 1) && column != this.column)
                return false;
            if (!(board[row, column] == null || board[row, column].Isblack() == !this.black))
                return false;

            return true;

        }
        
        public override void Move(int row2, int col2)
        {
            base.Move(row2, col2);
            moved = true;
        }
    }
}
