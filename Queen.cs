﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chess
{
    class Queen:Piece
    {
         public Queen(string symbol, bool black, int row, int col,Piece[,] board) : base(symbol, black, row, col,board) { }

         public override bool CanMoveTo(int row, int column)
         {
             bool canMoveR,canMoveB;
             canMoveR=CanMoveLikeRook(row, column);
             canMoveB=CanMoveLikeBishop(row, column);
             if (canMoveR || canMoveB)
                 return true;

             return false;
         }

         public bool CanMoveLikeBishop(int row, int column)
         {
             if (row + column != this.row + this.column && column - row != this.column - this.row)
                 return false;
             if (board[row, column] != null && board[row, column].Isblack() == this.Isblack())
                 return false;
             //עכשיו אעשה בדיקה האם לא היה שום כלי בדרך
             
             
             if (this.row < row) //התא שאני רוצה לעבור אליו מתחתיי-זא אני ב2 האפשריות של לעבור באלכסון למטה
             {
                 if (this.column < column)
                 {
                     for (int i = this.row+1, j =this.column+1 ; i < row; i++, j++) // אלכסון צד ימין
                         if (board[i, j] != null)
                             return false;
                 }
                 else
                 {
                     for (int i = this.row + 1, j = this.column -1; i < row; i++, j--) //אלכסון צד שמאל
                         if (board[i, j] != null)
                             return false;
                 }
             }
             else // זא אני באלכסון עליון ויש לי 2 אפשרויות-הכוונה התא שאני רוצה לעבור אליו מעליי
             {
                 if (this.column < column)
                 {
                     for (int i = this.row -1, j = this.column + 1; i > row; i--, j++) // אלכסון צד ימין
                         if (board[i, j] != null)
                             return false;
                 }
                 else
                 {
                     for (int i = this.row - 1, j = this.column - 1; i > row; i--, j--) //אלכסון צד שמאל
                         if (board[i, j] != null)
                             return false;
                 }
             }
             return true;
         }

         public bool CanMoveLikeRook(int row, int column)
         {
             if (this.row != row && this.column != column)
                 return false;

             if (board[row, column] != null && board[row, column].Isblack() == this.Isblack()) //&& הוא בסדר קדומיות נמוכה, אז קודם יבצע את את המשפט הראשון ,במידה ונכון ימשיך לבדוק את השני
                 return false;

             if (this.row == row)
             {

                 int minCol = this.column < column ? this.column : column;
                 int maxCol = this.column > column ? this.column : column;
                 for (minCol = minCol + 1; minCol < maxCol; minCol++)
                     if (board[row, minCol] != null)
                         return false;
             }
             else
             {
                 int minRow = this.row < row ? this.row : row;
                 int maxRow = this.row > row ? this.row : row;
                 for (minRow = minRow + 1; minRow < maxRow; minRow++)
                     if (board[minRow, column] != null)
                         return false;
             }
             return true;
         }
        
    }
}
