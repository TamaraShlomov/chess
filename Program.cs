﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chess
{
    class Program
    {
        static void Main(string[] args)
        {
            Chess chess = new Chess();
            chess.StartGame();
        }

    }
    class Chess
    {
        Piece[,] board;
        bool blackTurn;
        Piece W_king;
        Piece B_king;
        bool isShah;
        int no_eat_or_pawn_move_counter;
        string[] historyMoves;
        int historyMoves_counter;
        bool isTest = true;

        public Chess()
        {
            board = new Piece[8, 8];
            board[0, 0] = new Rook("B.R", true, 0, 0, board);
            board[0, 1] = new Knight("B.N", true, 0, 1, board);
            board[0, 2] = new Bishop("B.B", true, 0, 2, board);
            board[0, 3] = new Queen("B.Q", true, 0, 3, board);
            board[0, 4] = new King("B.K", true, 0, 4, board);
            board[0, 5] = new Bishop("B.B", true, 0, 5, board);
            board[0, 6] = new Knight("B.N", true, 0, 6, board);
            board[0, 7] = new Rook("B.R", true, 0, 7, board);
            for (int col = 0; col < board.GetLength(0); col++)
                board[1, col] = new Pawn("B.P", true, 1, col, board);


            board[7, 0] = new Rook("W.R", false, 7, 0, board);
            board[7, 1] = new Knight("W.N", false, 7, 1, board);
            board[7, 2] = new Bishop("W.B", false, 7, 2, board);
            board[7, 3] = new Queen("W.Q", false, 7, 3, board);
            board[7, 4] = new King("W.K", false, 7, 4, board);
            board[7, 5] = new Bishop("W.B", false, 7, 5, board);
            board[7, 6] = new Knight("W.N", false, 7, 6, board);
            board[7, 7] = new Rook("W.R", false, 7, 7, board);
            for (int col = 0; col < board.GetLength(0); col++)
                board[6, col] = new Pawn("W.P", false, 6, col, board);

            W_king = board[7, 4];
            B_king = board[0, 4];

            historyMoves = new string[1000]; //כיוון שלא למדנו עדיין ליצור מערך ללא מתן גודל מדוייק אז בחרתי מספר 1000 שאני יודעת שלא נגיע אליו
            
            this.Print();

        }

        public void Print()
        {
            if (isTest)
                Console.WriteLine();

            Console.WriteLine("     A     B     C     D     E     F     G      H ");
            for (int row = 0; row < board.GetLength(0); row++)
            {
                Console.WriteLine("---------------------------------------------------");
                Console.Write(row + 1 + " ");
                for (int col = 0; col < board.GetLength(0); col++)
                    Console.Write(board[row, col] != null ? "| " + board[row, col].getSymbol() + " " : "|     ");
                Console.Write("|");
                Console.WriteLine();
            }
            Console.WriteLine("---------------------------------------------------");
        }

        public void StartGame()
        {
            bool gameOver = false;

            int row1 = -1, col1 = -1, row2 = -1, col2 = -1;
            string input;

            while (!gameOver)
            {
                 
                  if (this.Tie())
                {
                    Console.WriteLine("Game over, its a tie !!!");
                    gameOver = true;
                    continue;
                }
                
                string turn = blackTurn ? "Black turn" : "White Turn";
                Console.WriteLine(turn);

                while (true)
                {
                    Console.Write("Move piece from: ");
                    input = isTest ? Testing.GetNextMove() : Console.ReadLine();
                    if (isTest && input == null)
                    {
                        gameOver = true;
                        break;
                    }
                    input = input.ToUpper();
                    if (IsValidCell(input))
                    {
                        row1 = int.Parse(input[1] + "") - 1;
                        col1 = input[0] - 65;
                        if (board[row1, col1] == null)
                        {
                            Console.WriteLine("The cell is empty,try again");
                            continue;
                        }
                        if (board[row1, col1].Isblack() != blackTurn)
                        {
                            Console.WriteLine("You must choose piece in your color,try again");
                            continue;
                        }
                        break;
                    }
                    else
                        Console.WriteLine("invalid input,the syntax must be like this 'A'-'H'1-8 ,try again");
                }


                if (gameOver)
                    break;

                while (true)
                {
                    Console.Write("To: ");
                    input = isTest ? Testing.GetNextMove() : Console.ReadLine();
                    input = input.ToUpper();
                    if (isTest && input == null)
                    {
                        gameOver = true;
                        break;
                    }

                    if (IsValidCell(input))
                    {
                        row2 = int.Parse(input[1] + "") - 1;
                        col2 = input[0] - 65;
                        break;
                    }
                    else
                    {
                        if (input == "CASTLING" && board[row1, col1] is Rook)
                            break;
                        Console.WriteLine("invalid input,the syntax must be like this 'A'-'H'1-8 or 'castling' if your piece was rook' ,try again");
                    }

                }

                if (gameOver)
                    break;



                bool canmove = false;
                bool canDoTheCastling = false;
                if (input == "CASTLING")
                    canDoTheCastling = this.Castling(blackTurn ? B_king : W_king, board[row1, col1]);
                else
                {
                    if (board[row1, col1] is King && row1 == row2 && Math.Abs(col1 - col2) == 2)
                    {
                        Console.Write("Do you want to do castling? press y/n: ");
                        bool castling = Console.ReadLine() == "y".ToUpper() ? true : false;
                        if (castling)
                        {
                            col2 = col1 - col2 == 2 ? 0 : 7;
                            canDoTheCastling = this.Castling(board[row1, col1], board[row2, col2]);
                        }
                        else
                            canmove = false;
                    }
                    else
                        canmove = board[row1, col1].CanMoveTo(row2, col2);
                }

                if (canmove)
                {

                    if (!this.ValidMove(row1, col1, row2, col2, blackTurn))
                    {
                        Console.WriteLine("illigal action,because your king is vulnerable to attacks,try again");
                        continue;
                    }

                    if (board[row2, col2] != null || board[row1, col1] is Pawn) //אם התזוזה היא הכאה או הזזת רגלי אז תאפס לי את הקאונטר
                        no_eat_or_pawn_move_counter = 0;
                    else
                        no_eat_or_pawn_move_counter++;

                    board[row1, col1].Move(row2, col2);


                    if (this.CanDoCoronation(row2, col2))
                    {
                        this.Coronation(row2, col2);
                    }

                    ///////////
                    string color = blackTurn ? "B" : "W";
                    historyMoves[historyMoves_counter] = "" +color+ row1 + col1 + row2 + col2;
                    historyMoves_counter++;
                    //////////

                    Print();
                }
                else if (canDoTheCastling)
                {
                    ///////////////////////
                    no_eat_or_pawn_move_counter++; //התזוזה היא הצרחה אז נקדם את הקאונטר
                   
                    string color = blackTurn ? "B" : "W";
                    if (input == "castling")
                        historyMoves[historyMoves_counter] = "" +color+ row1 + col1 + input;
                    else
                        historyMoves[historyMoves_counter] = "" +color+ row1 + col1 + row2 + col2;

                    historyMoves_counter++;
                    /////////////////////////
                    Print();
                }
                else
                {

                    Console.WriteLine("iligal action");
                    continue;
                }

                int opponent_kingLocationRow = !blackTurn ? B_king.getRow() : W_king.getRow();
                int opponent_KingLocationCol = !blackTurn ? B_king.getColumn() : W_king.getColumn();

                isShah = this.Shah(opponent_kingLocationRow, opponent_KingLocationCol, blackTurn); //בודק האם אני שם את היריב שלי במצב שח
                if (isShah)
                {
                    if (this.Mat(!blackTurn)) //בודק האם יש מט על הצבע שהעברתי(של היריב
                    {
                        string winner = blackTurn ? "Black" : "White" + " player";
                        Console.WriteLine("Shah-Math , game over, winner: " + winner);
                        gameOver = true;
                        break;
                    }

                    Console.WriteLine("Shah");
                }

                blackTurn = !blackTurn;
            }

        }

        public bool IsValidCell(string input)
        {
            if (input.Length != 2)
                return false;
            if (input[0] < 65 || input[0] > 72)
                return false;
            if (input[1] < 49 || input[1] > 56)
                return false;

            return true;
        }

        public bool CanDoCoronation(int row, int col)
        {
            if (board[row, col] is Pawn)
            {
                if (board[row, col].Isblack())
                {
                    if (row == 7)
                        return true;
                }
                else
                {
                    if (row == 0)
                        return true;
                }
            }
            return false;
        }
         
        public void Coronation(int row, int col)
        {
            Console.Write("Do you want to do coronation? press y/n :");
            string result = Console.ReadLine();
            if (result == "n")
                return;
            Console.Write("Press one of this Options: Queen,Bishop,Knight,Rook : ");
            result = Console.ReadLine();
            string color = blackTurn ? "B" : "W";
            switch (result)
            {
                case "Queen":
                    board[row, col] = new Queen(color + ".Q", blackTurn, row, col, board);
                    break;
                case "Bishop":
                    board[row, col] = new Bishop(color + ".B", blackTurn, row, col, board);
                    break;
                case "Knight":
                    board[row, col] = new Knight(color + ".N", blackTurn, row, col, board);
                    break;
                case "Rook":
                    board[row, col] = new Rook(color + ".R", blackTurn, row, col, board);
                    break;
            }
        }

        public bool Castling(Piece King, Piece piece2)
        {
            if (((King)King).Ismoved() || ((Rook)piece2).Ismoved())
                return false;

            if (piece2.getColumn() < 4)
            {
                for (int col = 3; col > 0; col--)
                    if (board[King.getRow(), col] != null)
                        return false;

                for (int col = 4; col >= 2; col--)
                    if (this.Shah(King.getRow(), col, !blackTurn))
                        return false;

                King.Move(King.getRow(), 2);
                piece2.Move(piece2.getRow(), 3);


            }
            else
            {
                for (int col = 5; col < 7; col++)
                    if (board[King.getRow(), col] != null)
                        return false;

                for (int col = 4; col <= 6; col++)
                    if (this.Shah(King.getRow(), col, !blackTurn))
                        return false;

                King.Move(King.getRow(), 6);
                piece2.Move(piece2.getRow(), 5);
            }

            return true;
        }

        public bool Shah(int kingLocationRow, int KingLocationCol, bool blackTurn)
        {

            bool result;
            for (int row = 0; row < 8; row++)
                for (int col = 0; col < 8; col++)
                    if (board[row, col] != null && board[row, col].Isblack() == blackTurn)
                    {
                        result = board[row, col].CanMoveTo(kingLocationRow, KingLocationCol); // check if this piece can eat the king
                        if (result)
                            return true;
                    }

            return false;

        }

        public bool Mat(bool blackTurn)
        {

            for (int row1 = 0; row1 < 8; row1++)
                for (int col1 = 0; col1 < 8; col1++)
                {
                    if (board[row1, col1] != null && board[row1, col1].Isblack() == blackTurn)
                        for (int row2 = 0; row2 < 8; row2++)
                            for (int col2 = 0; col2 < 8; col2++)
                                if (board[row1, col1].CanMoveTo(row2, col2))
                                    if (this.ValidMove(row1, col1, row2, col2, blackTurn))
                                        return false;
                }

            return true;
        }

        public bool ValidMove(int row1, int col1, int row2, int col2, bool blackTurn)
        {
            Piece prev = board[row2, col2];

            bool moved = false;
            bool isdo2steps = false;
            bool isValidMove = true;
            Piece prevPawn=null;
            if (board[row1, col1] is King)
                moved = ((King)board[row1, col1]).Ismoved();

            if (board[row1, col1] is Rook)
                moved = ((Rook)board[row1, col1]).Ismoved();

            if (board[row1, col1] is Pawn)
            {
                isdo2steps = Pawn.getPreviousTurn2Steps();
                if (((Pawn)board[row1, col1]).CanDoEn_passant(row2, col2))
                    prevPawn = Pawn.prevPawn;
            }
            
            board[row1, col1].Move(row2, col2);
            //בדיקה האם ההזזה שמה את המזיז במצב של שח
            if (this.Shah(blackTurn ? B_king.getRow() : W_king.getRow(), blackTurn ? B_king.getColumn() : W_king.getColumn(), !blackTurn))
                isValidMove = false;////

            board[row2, col2].Move(row1, col1);

            if (board[row1, col1] is King)
                ((King)board[row1, col1]).setMoved(moved);

            if (board[row1, col1] is Rook)
                ((Rook)board[row1, col1]).setMoved(moved);

            if (board[row1, col1] is Pawn)
            {
                Pawn.setPreviousTurn2Steps(isdo2steps);
                if(prevPawn!=null)
                board[blackTurn ? row2 - 1 : row2 + 1, col2] = prevPawn;
            }

            board[row2, col2] = prev;


            return isValidMove;
        }

        public bool Stalemate() //פט
        {
            //__________________________אם הצד שתורו לשחק לא יכול לבצע אף תזוזה חוקית ,אך אינו נמצא במצב של שח__________________________//

            if (!isShah && Mat(blackTurn))
                return true;

            return false;

        }

        public bool Not_enough_material_to_make_mat()
        {
            //__________________________אם נשאר בלוח מלך מלך או מלך רץ מול מלך -לפי ההסבר באנגלית__________________________//
            int counter = 0;
            for (int row = 0; row < 8; row++)
                for (int col = 0; col < 8; col++)
                    if (board[row, col] != null)
                    {
                        if (!(board[row, col] is King || board[row, col] is Bishop))
                            return false;
                        counter++;
                    }
            if (counter == 2 || counter == 3)
                return true;

            return false;
        }

        public bool Same_posotion_3_times()
        {
            int numbers_of_time_this_posotin_was = 0;
            if (historyMoves_counter == 0)
                return false;
            string lastPosotion = historyMoves[historyMoves_counter-1];
            for (int index = 0; index < historyMoves_counter; index++)
                if (historyMoves[index] == lastPosotion)
                    numbers_of_time_this_posotin_was++;

            if (numbers_of_time_this_posotin_was == 3)
                return true;

            return false;
        }

        public bool Prepormed_50_moves_without_eating_or_move_pawn()
        {
            if (no_eat_or_pawn_move_counter == 50)
                return true;

            return false;
        }

        public bool Tie()
        {
            return Stalemate() || Not_enough_material_to_make_mat() || Same_posotion_3_times() || Prepormed_50_moves_without_eating_or_move_pawn();
        }

    }
}

